#include "ofApp.h"
#include <vector>
#include <iostream>

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(0);
	for (int i = 0; i < 15; i++)
	{
		ofColor(255);
		ofDrawRectangle(rand() % 100, rand() % 100, 5, 5);
	};

	shader.load("nebula");
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
	//ofDrawRectangle(rand() % 100, rand() % 100, 5, 5);
	ofSetColor(255);
	shader.begin();
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
	shader.end();
	
	for (int i = 0; i < 15; i++)
	{
		ofColor(255);
		starx.push_back(rand() % ofGetWindowWidth());
		stary.push_back(rand() % ofGetWindowHeight());
	};

	for (int i = 0; i < 15; i++)
	{
		ofColor(255);
		ofDrawRectangle(starx[i], stary[i], 5, 5);
	};
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
